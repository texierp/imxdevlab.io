---
title: How to contribute
redirect_from: blog/2016/09/13/How_to_contribute/
date: 2016-09-13 11:00:57
author: andres
categories: [ Blog ]
tags: [ ]
---
Thanks for reading our blog. We are happy to accept your posts! Below are some guidelines for you to be part of our community.

## Cloning the I.MXDev source code

The first thing you need to do is to clone the source code of our blog. Please follow the steps below to do so:

1) The repository for our blog here can be found here:

<https://gitlab.com/imxdev/imxdev.gitlab.io>

In order to send the merge request and add your post you would need to fork the blog source code on your profile. The link below shows the process:

<https://docs.gitlab.com/ee/workflow/forking_workflow.html>

Specifics on the forking process can also be found here:

<https://docs.gitlab.com/ee/gitlab-basics/fork-project.html>

2) Once you forked the blog source code, download it to your machine using the command below:

```console
$git clone <url_of_your_gitlab_fork>
```

3) Once the clonning process is compelte, move into the blog folder

```console
$cd imxdev.gitlab.io/
```

### How to add an author

The first step is to add yourself as an author. In order to do so, please follow the steps below:

1) Open the file _data/authors.yml using your favorite text editor.

2) Add your information on the authors section. Please maintain alphabetical order by first name. You can follow the examples of the current list of authors or define your profile with the arguments found [here](https://gitlab.com/imxdev/imxdev.gitlab.io/blob/master/_config.yml#L78)

* name
* bio
* gitlab

This list reflects the minimal requirements for a new author.

3) Save the document and push the change by running the commands below:

```console
$ git add _data/authors.yml
$ git commit -s -m "Add myself as an author"
$ git push origin master:master
```
5) Follow the tutorial below to send a merge request on GitLab with your new author commit:

<https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html>

The commit example below shows a new author being added:

<https://gitlab.com/imxdev/imxdev.gitlab.io/commit/3f4749ec6d724020e464129d57c241b621ca2a6f>

### Sending a new post:

1) In order to create a new post with the correct heading please run the following script:

```console
$ ./scripts/new_post.sh
```

This script will prompt you for the title of your post and your author's name. You should use the same name you use on your new author commit, usually your first name in lowercase.

After you run the script you can find your new post under the _posts folder

2) Use your favorite editor to write your new post. We use Markdown to write our posts.

3) Once you finish writing your post you can push it to our repository using the commands below:

```console
$ git add _posts/(name of your post)
$ git commit -s -m "NP: <title>"
$ git push origin master:master
```

7) As you did with the new author commit you can follow the tutorial below to send a merge request on GitLab with your post:

<https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html>



### Reporting Issues

If you find an issue with our blog please go to the link below and follow the steps to report. We greatly appreciate your help!

<https://gitlab.com/imxdev/imxdev.gitlab.io/issues>





