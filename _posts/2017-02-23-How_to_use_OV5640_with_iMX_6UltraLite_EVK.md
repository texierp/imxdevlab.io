---
title: How to use OV5640 with i.MX 6UltraLite EVK
date: 2017-02-23 16:11:35
author: marco
categories: [ Tutorial ]
tags: [ L4.1.15_2.0.0_GA, GStreamer, IMX6UL, ov5640 ]
---
The i.MX 6UltraLite EVK kernel release includes the parallel camera OV5640 by default. However, this camera requires a 700-27820 adapter:

<center><img src="{{ base_path }}/images/camera_adapter.png" alt="Camera Adapter"  width="300"/></center>
<center><figcaption>Camera Adapter</figcaption></center>

Plug in the camera with the adapter and connect the other end of the adapter to the board:

<center><img src="{{ base_path }}/images/camera_position.png" alt="Camera position" width="300"/></center>
<center><figcaption>Camera position</figcaption></center>

In order to use the parallel OV5640 camera, it's necessary to set up the environment variable below on U-Boot:

```
=> setenv fdt_file ‘imx6ul-14x14-evk-csi.dtb’
=> saveenv
```
Follow the GStreamer pipeline example to test the camera connection:
```
$ gst-launch-1.0 v4l2src device=/dev/video1 ! video/x-raw,width=640,height=480 ! autovideosink
```

<center><img src="{{ base_path }}/images/camera_test.png" alt="Camera test" width="300"/></center>
<center><figcaption>Camera test</figcaption></center>

This test was done using the kernel BSP release 4.1.15-2.0.0 GA.
