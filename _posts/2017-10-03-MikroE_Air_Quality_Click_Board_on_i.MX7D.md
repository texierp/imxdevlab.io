---
title: MikroE Air Quality Click Board on i.MX7D
date: 2017-10-03 13:07:56
author: marco
categories: [ Tutorial ]
tags: [ MikroE, IMX7D, air_quality_click, ADC, analog_pin, Click_boards, L4.1.15_2.0.0_GA]
---
This tutorial shows how to use the [Air Quality Click board](https://shop.mikroe.com/air-quality-click) on the i.MX 7Dual SABRE-SD and its mikroBUS compatible socket.

According to the [MikroE description](https://shop.mikroe.com/air-quality-click), the Air quality click is a simple solution for detecting a variety of gases that impact air quality. Composed by a MQ-135 sensor, this click board is designed to run on a 5V power supply and communicates with the target microcontroller over the **AN** pin on the mikroBUS line.

The mikroBUS **AN** pin in i.MX 7Dual SABRE-SD is configured to the **ACD0** - channel 0 (**ACD0_IN0**), an analog-to-digital converter pin enabled by default in the `imx7-sdb.dts` file.

To read the **ACD0_IN0** value, boot the board and enter the command line below:
```console
$ cat /sys/bus/iio/devices/iio\:device0/in_voltage0_raw
```

The value from this command is the results of the formula:

`Vread = Vin * 4095 / Vref`

where:
- `Vread` is the digital value actually read;
- `Vin` is the analog input voltage we want;
- `4095` is 2^12 bits (4096) words of word-addressable memory;
- `Vref` is the ADC voltage reference, which is 1.8V on i.MX 7D SoCs.

To simplify the formula, you can read the **in_voltage_scale**:
```console
$ cat /sys/bus/iio/devices/iio\:device0/in_voltage_scale
```
This value means `Vref / 4095`. In this case, `1800 mV / 4095 = 0.4395`.

So `Vin` can be calculated just by:

`Vin = Vread * 0.4395`

You can use the follow <a href="{{base_path}}/scripts/air_quality.sh">script</a> to get a continuous application of this formula, used to measure the polluting gases concentration in the air in ppm:
```shell
# /bin/sh

while :
do
        num=$(cat /sys/bus/iio/devices/iio\:device0/in_voltage0_raw)
        echo "$(($num * 1800 / 4095)) ppm"
        sleep 1
done
```

The polluting gases concentration is detected by the MQ-135 sensor, which has a layer made of tin dioxide (SnO2) that has lower conductivity in clean air than in polluting gases presence. The following video shows a demonstration of ppm variance caused by the lighter gas and smoke:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jnVv4Vo_mR8" frameborder="0" allowfullscreen></iframe>
