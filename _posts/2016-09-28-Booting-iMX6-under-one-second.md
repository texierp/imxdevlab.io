---
title:  Booting i.MX6 under one second
redirect_from: blog/video/tutorial/2016/09/28/Booting-iMX6-Under-One-Second/
date: 2016-09-28 14:42:28
author: diego
categories: [ Video, Tutorial ]
tags: [ fast-boot, uboot, falconmode, IMX6Q ]
---

I'm running the NXP **i.MX6Q SDB** using U-Boot Falcon mode to reduce booting time.
I currently managed to boot a complete Linux image up to the login prompt in about 0.9 second.

The links are available below:

* Video on [Youtube](https://www.youtube.com/watch?v=Zl-Ypk9S-dc).

<iframe width="560" height="315" src="https://www.youtube.com/embed/Zl-Ypk9S-dc" frameborder="0" allowfullscreen align="center"></iframe>

* Pre-built image on [Dropbox](https://www.dropbox.com/s/d6vgztz701blbmc/falconmode_imx6sabresd.sdcard?dl=0).
* The statistical analysis is on [Plotly](https://plot.ly/~diegohdorta/0/embedded-linux-boot-time-optimizations-imx6q/).

To upload the image into the SD card, use the following command:

```console
$ sudo dd if=falconmode_imx6qsabresd.sdcard of=/dev/sdX ; sync
```

The tutorial for enabling Falcon mode is available [here](https://imxdev.gitlab.io/tutorial/How_to_decrease_boot_time_on_iMX6/).
