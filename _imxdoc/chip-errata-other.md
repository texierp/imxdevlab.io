---
title: "i.MX SoC Errata"
excerpt: "A page to list all the links to the online Errata for the imx chips"
---
{% include imxdoc/rm-introduction.md %}
{% include imxdoc/ce-imxother-table.md %}
